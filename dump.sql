CREATE TABLE `img` (
	`id` int PRIMARY KEY AUTO_INCREMENT,
	`first_name` varchar(255),
	`last_name` varchar(255),
	`address` longtext,
	`postcode` varchar(255),
	`phone` varchar(255)
);

CREATE TABLE `order_product` (
	`id` int PRIMARY KEY AUTO_INCREMENT,
	`product_id` int,
	`order_id` int,
	
	FOREIGN KEY(`product_id`) REFERENCES `products` (`id`),
	FOREIGN KEY(`order_id`) REFERENCES `orders` (`id`)
);

INSERT INTO `img`.`products` (`name`, `picture`, `id`) VALUES ('Awesome product', '/img/derniere.jpg', '132.99');
INSERT INTO `img`.`products` (`name`, `picture`, `id`) VALUES ('Regular Product', '/img/germanium.jpg', '58.59');
INSERT INTO `img`.`products` (`name`, `picture`, `id`) VALUES ('So-so Product', '/img/malachite.jpg', '8.99');

